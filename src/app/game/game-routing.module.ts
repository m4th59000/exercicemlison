import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GamesComponent} from './containers/games/games.component';
import {GameDetailsComponent} from './containers/game-details/game-details.component';

const routes: Routes = [
  {path: ':id/details', component: GameDetailsComponent},
  {path: '', component: GamesComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GameRoutingModule { }
