import{Component}from'@angular/core';
import {Game}from '../../models/game';


@Component({
selector: 'val-games',
templateUrl: './games.component.html',
styleUrls: ['./games.component.sass']
})
export class GamesComponent {
gameList = [
{title: 'F-Zero', releaseDate: 'Mai 15, 2021', price: 59.99},
{title: 'Tenchu', releaseDate: 'Aout 09, 1999', price: 9.99},
{title: 'SuperMario', C: 'Octobre 25, 2012', price: 12.99},
{title: 'MarioKart', releaseDate: 'Septembre 30, 2017', price: 47.99},
{title: 'Skyrim', releaseDate: 'Novembre 02, 2015', price: 24.99},
{title: 'StreetOfRage', releaseDate: 'Janvier 28, 2013', price: 16.99},
{title: 'DeadIsland', releaseDate: 'Mars 17, 1992', price: 4.99},
];


showDetails(game:Game):void{
console.log(game);
}
}
