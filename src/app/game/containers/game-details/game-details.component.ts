import {Component} from '@angular/core';

@Component({
  selector: 'val-game-details',
  templateUrl: './game-details.component.html',
  styleUrls: ['./game-details.component.sass']
})
export class GameDetailsComponent {
  games = { title: 'StreetFighter', releaseDate: 'Jun 15, 1992', price: '9.99' };
}
