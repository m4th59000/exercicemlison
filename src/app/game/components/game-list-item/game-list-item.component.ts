import {Component, EventEmitter, Input, Output} from '@angular/core';
@Component({
  selector: 'val-game-list-item',
  templateUrl: './game-list-item.component.html',
  styleUrls: ['./game-list-item.component.sass']
})
export class GameListItemComponent {
  @Input() gameListItem: string;
  @Output() onGameClick = new EventEmitter();



  selectGame() {
    this.onGameClick.emit(this.gameListItem);
    console.log(this.gameListItem);
  }
}
