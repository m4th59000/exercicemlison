import { Component, OnInit, Input } from '@angular/core';
import {map, tap, takeWhile, endWith} from 'rxjs/operators';
import {interval} from 'rxjs';

@Component({
  selector: 'app-simple-counter',
  templateUrl: './simple-counter.component.html',
  styleUrls: ['./simple-counter.component.scss']
})
export class SimpleCounterComponent{
@Input() startAt;
@Input() endAt;
@Input() step;
@Input() delay;
@input() digits;

currentValue;


reset(){
interval(this.delay).pipe(
 map(x => this.startAt + x * this.step),
 tap(x => console.log(x)),
 takeWhile(x => x < this.endAt),
 endWith(this.endAt),
).subscribe(number=>this.currentValue =number);
}
ngOnDestroy() {
    }
}
