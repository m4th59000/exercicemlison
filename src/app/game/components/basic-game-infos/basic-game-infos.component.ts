import {Component, Input} from '@angular/core';

@Component({
  selector: 'val-basic-game-infos',
  templateUrl: './basic-game-infos.component.html',
  styleUrls: ['./basic-game-infos.component.sass']
})
export class BasicGameInfosComponent {
  @Input() gameInfos;
}
